export default {
  namespaced: true,
  state: {
    id: 0,
    name: '',
    logo: '',
    type: ''
  },
  mutations: {
    updateId (state, id) {
      state.id = id
    },
    updateName (state, name) {
      state.name = name
    },
    updateLogo (state, logo) {
      state.logo = logo
    },
    updateType (state, type) {
      state.type = type
    }
  }
}
