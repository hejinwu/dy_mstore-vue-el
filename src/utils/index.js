import router from '@/router'
import store from '@/store'
import Vue from 'vue'

/**
 * 获取uuid
 */
export function getUUID () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    return (c === 'x' ? (Math.random() * 10 | 0) : ('r&0x3' | '0x8')).toString(10)
  })
}

/**
 * 获取当前日期
 * yyyyMMdd
 */
export function getNowDate () {
  var nowDate = new Date()
  var year = nowDate.getFullYear()
  var month = nowDate.getMonth() + 1 < 10 ? '0' + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1
  var day = nowDate.getDate() < 10 ? '0' + nowDate.getDate() : nowDate.getDate()
  return year + '' + month + '' + day
}

/**
 * 获取文件后缀
 */
export function getSuffix (fileName) {
  return fileName.substring(fileName.lastIndexOf('.'))
}

/**
 * 是否有权限
 * @param {*} key
 */
export function isAuth (key) {
  return JSON.parse(sessionStorage.getItem('permissions') || '[]').indexOf(key) !== -1 || false
}

/**
 * 级联选择器转换
 * @param {*} data
 * @param {*} id
 * @param {*} pid
 */
export function cascaderDataTranslate (data, id = 'id', pid = 'parentId') {
  var res = []
  var temp = {}
  for (var i = 0; i < data.length; i++) {
    temp[data[i][id]] = data[i]
  }
  for (var k = 0; k < data.length; k++) {
    if (temp[data[k][pid]] && data[k][id] !== data[k][pid]) {
      if (!temp[data[k][pid]]['children']) {
        temp[data[k][pid]]['children'] = []
      }
      temp[data[k][pid]]['children'].push(data[k])
    } else {
      res.push(data[k])
    }
  }
  return res
}

/**
 * 树形数据转换
 * @param {*} data
 * @param {*} id
 * @param {*} pid
 */
export function treeDataTranslate (data, id = 'id', pid = 'parentId') {
  var res = []
  var temp = {}
  for (var i = 0; i < data.length; i++) {
    temp[data[i][id]] = data[i]
  }
  for (var k = 0; k < data.length; k++) {
    if (temp[data[k][pid]] && data[k][id] !== data[k][pid]) {
      if (!temp[data[k][pid]]['children']) {
        temp[data[k][pid]]['children'] = []
      }
      if (!temp[data[k][pid]]['_level']) {
        temp[data[k][pid]]['_level'] = 1
      }
      data[k]['_level'] = temp[data[k][pid]]._level + 1
      temp[data[k][pid]]['children'].push(data[k])
    } else {
      res.push(data[k])
    }
  }
  return res
}

/**
 * 清除登录信息
 */
export function clearLoginInfo () {
  Vue.cookie.delete('token')
  store.commit('resetStore')
  router.options.isAddDynamicMenuRoutes = false
}

/**
 * 中国标准时间转换
 * @param  {*} str [Wed Aug 01 2018 19:04:23 GMT+0800]
 * @return {*}     [yyyy-MM-dd HH:mm:ss]
 */
export function dateTimeFormat (str) {
  if (str == null || str === '') {
    return ''
  }
  var date = new Date(str)
  var dateValue = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()
  return dateValue
}

/**
 * 中国标准时间转换
 * @param  {*} str [Wed Aug 01 2018]
 * @return {*}     [yyyy-MM-dd]
 */
export function dateFormat (str) {
  if (str == null || str === '') {
    return ''
  }
  var date = new Date(str)
  var dateValue = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
  return dateValue
}
