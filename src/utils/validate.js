/**
 * 邮箱
 * @param {*} s
 */
export function isEmail (s) {
  return /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(s)
}

/**
 * 手机号码
 * @param {*} s
 */
export function isMobile (s) {
  return /^1[0-9]{10}$/.test(s)
}

/**
 * 电话号码
 * @param {*} s
 */
export function isPhone (s) {
  return /^([0-9]{3,4}-)?[0-9]{7,8}$/.test(s)
}

/**
 * 联系方式
 * @param {*} s
 */
export function isContact (s) {
  return /^1[0-9]{10}$/.test(s) || /^([0-9]{3,4}-)?[0-9]{7,8}$/.test(s)
}

/**
 * 正整数
 * @param {*} s
 * */
export function isNumber (s) {
  return /^[1-9]\d*$/.test(s)
}

/**
 * 价格（正数两位小数）
 * @param {*} s
 */
export function isPrices (s) {
  return /^([1-9]\d*|0)(\.\d{1,2})?$/.test(s)
}

/**
 * URL地址
 * @param {*} s
 */
export function isURL (s) {
  return /^http[s]?:\/\/.*/.test(s)
}

/**
 * 数组a包含数组b
 * @param {*} a
 * @param {*} b
 */
export function isContained (a, b) {
  if (!(a instanceof Array) || !(b instanceof Array)) { return false }
  if (a.length < b.length) { return false }
  var aStr = JSON.stringify(a)
  for (var i = 0, len = b.length; i < len; i++) {
    const v = JSON.stringify(b[i])
    if (aStr.indexOf(v) === -1) { return false }
  }
  return true
}
